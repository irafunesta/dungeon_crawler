﻿using UnityEngine;
using System.Collections;

public enum DamageType
{
    PHYSICAL,
	FIRE,
	WATER,
	EARTH,
	WIND,
	LIGHT,
	DARK
}

public enum TargetType
{
	PLAYER,
	ENEMY,
}

//public enum SpellsName
//{
//	HEAL, 
//	MEGAHEAL,
//	METEOR
//}

//public enum SpellEffect
//{
//	CURE_TARGET,
//	DAMAGE_TARGET
//}

//public class EvMessage 
//{
//	public SpellEffect effect;
//	public TargetType target;
//	public int cured_amount = 0;
//	public int damage_amount = 0;
//}

public class SpellManager : MonoBehaviour
{
	public static SpellManager instance {private set; get;}
    
	public Spell[] spells;

	// Use this for initialization
	void Start ()
	{
		if(instance != null && instance != this)
		{
			// If that is the case, we destroy other instances
			Destroy(gameObject);
		}

		// Here we save our singleton instance
		instance = this;

		//EventManager.UseSpell += UseSpell;
	}

//	public void UseSpell(Spell spell)
//	{
//        Debug.Log("launched spell: " + spell.a_name);

//        foreach (GameEvent ev in spell.event_trigger)
//		{
            
//			switch(ev)
//			{
//				case GameEvent.CURE_PLAYER:
//					EventManager.EmitEvent (ev, null, spell.s_heal);
//					break;
//				case GameEvent.UPDATE_UI:
//					EventManager.EmitEvent (ev, null);
//					break;
//				case GameEvent.UI_SHOW_HEAL_NUMBER:
//					EventManager.EmitEvent (ev, null, spell.s_heal);
//					break;
//                case GameEvent.DAMAGE_ENEMY:
//                    EventManager.EmitEvent(ev, null, spell.s_damage);
//                    break;
//                case GameEvent.SHOW_EFFECT:
//                    EventManager.EmitEvent(ev, null);
//                    break;
//                default:
//					EventManager.EmitEvent (ev, null);
//					break;
//			}
////			EventManager.EmitEvent (ev, null, spell.);
//		}

//	}
}

