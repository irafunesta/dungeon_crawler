﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler  {

	public int slotNum = 0;
	//Next Step
	//public int quantity;
    //public ItemScriptable item;
    public Text quantity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		print ("Enter item in pos:" + slotNum.ToString ());
		Vector2 pos = new Vector2 ();

		RectTransform thisPos = gameObject.GetComponent<RectTransform> ();

		if(thisPos != null)
		{
			//pos = thisPos.anchoredPosition;
			//pos = new Vector2 (100f, 100f);
			//pos.y += GameController.instance.toolTipOffset;
		}

		GameController.instance.ShowTooltip (slotNum, false, pos);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		print ("Exit item ");
		GameController.instance.HideTooTip ();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		print ("Cliccked item in pos:" + slotNum.ToString () + " button" + eventData.button.ToString());

		//Equip
		if(eventData.button == PointerEventData.InputButton.Right)
		{			
			GameController.instance.DefaultItemAction (slotNum);
		}
		//else if (eventData.button == PointerEventData.InputButton.Left)
		//{
		//	GameController.instance.TestIncreaseScriptable (slotNum);
		//}
	}

	/*public void ClickInvetorySlot()
	{
		//Cliccked item
		print ("Cliccked item in pos:" + SlotNum.ToString ());
	}*/
}
