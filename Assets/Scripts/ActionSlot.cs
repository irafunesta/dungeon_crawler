﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum ActionType
{
	SPELL,
	SKILL,
	ITEM,
    RUNE_SPELL
}

public enum ActionTag
{
	CURE_PLAYER,
	HEAL_PLAYER,
	MEGA_HEAL_PLAYER,
	FIRE_DAMAGE,
	METEOR
}

//[CreateAssetMenu(fileName = "Action", menuName = "Actions/Action", order = 1)]
public abstract class Action : ScriptableObject
{	
    [Header("Action")]
	public string a_name;
	public ActionType type;

	public abstract void Activate (Target actionTarget = Target.NONE);
	//public abstract void Activate (ITarget target, Button btn);
}



