﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RuneSpell", menuName = "Magic/RuneSpell", order = 1)]
public class RuneSpell : Action {

    public string s_name_str;
    public string runinc_spell_word; //max 5
    public int s_damage;
    public int s_heal;
    public DamageType s_damage_type;
    public GameEvent[] event_trigger;

    public override void Activate(Target t)
    {
        //target.Heal(s_heal);
        //target.Damage(s_damage);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("heal_amount", s_heal.ToString());
        data.Add("damage_amount", s_damage.ToString());

        foreach (GameEvent e in event_trigger)
        {
            EventManager.EmitEvent(e, data);
        }

        Player p = EventManager.GetPlayerData();
        if(p != null)
        {
            GameController.instance.ShowMessage("player hp: " + p.hp.ToString(), 1);
        }
    }
}
