﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EquipSlot : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler {

	public int slotNum = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		print ("Enter item in pos:" + slotNum.ToString ());

		print ("Enter item in pos:" + slotNum.ToString ());
		Vector2 pos = new Vector2 ();

		RectTransform thisPos = gameObject.GetComponent<RectTransform> ();

		if(thisPos != null)
		{
			pos = thisPos.rect.position;
		}

		GameController.instance.ShowTooltip (slotNum, true, pos);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		print ("Exit item in pos:" + slotNum.ToString ());
		GameController.instance.HideTooTip ();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		print ("Cliccked item in pos:" + slotNum.ToString () + " button" + eventData.button.ToString());

		//Equip
		if(eventData.button == PointerEventData.InputButton.Right)
		{
			GameController.instance.UnEquipItem (slotNum);
		}
	}
}
