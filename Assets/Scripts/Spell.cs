﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Spell", menuName = "Magic/Spell", order = 1)]
public class Spell : Action
{
	//public SpellsName s_name;
	public string s_name_str;
	public string spell_word;
	public int s_damage;
	public int s_heal;
//	public int power;
	public TargetType target;
	public DamageType s_damage_type;
	public GameEvent[] event_trigger;

    public override void Activate(Target t)
    {
        //target.Heal(s_heal);
        //target.Damage(s_damage);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("heal_amount", s_heal.ToString());
        data.Add("damage_amount", s_damage.ToString());

        foreach (GameEvent e in event_trigger)
        {
            EventManager.EmitEvent(e, data);
        }
    }
}

