﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum EffectsName
{
	ATTACK,
	HEAL,
	PLAYER_DAMAGE
}

[Serializable]
public struct Effect
{
    public EffectsName name;
    public GameObject effect;
}

public class EffectManager : MonoBehaviour
{
    public Effect[] effectsList = new Effect[]
    {
        new Effect{name=EffectsName.ATTACK, effect = null}
    };
    // Use this for initialization
    void Start ()
	{
        EventManager.SHOW_EFFECT_Listener += ShowEffect;
    }
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void ShowEffect(string name)
	{
        for (int i = 0; i < effectsList.Length; i++)
        {
            Effect e = effectsList[i];
            if (e.name.ToString() == name && e.effect.activeSelf == false)
            {
                e.effect.SetActive(true);
                break;
            }
        }
	}
}

