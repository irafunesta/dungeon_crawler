﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MovePLayer : MonoBehaviour 
{
	//public GameObject falseRotation;
	public float cell_size = 50f;
	public float rotation = 90f;
	public float move_speed = 2f;
	public float turn_speed = 2f;

	private Ray rayFront;
	private Ray rayBack;
	//private Ray rayLeft;
	//private Ray rayRight;
	private Color ray_col = Color.red;

	Quaternion q;
	Vector3 direction = Vector3.forward;

	bool is_turning_left = false;
	bool is_turning_right = false;
	bool is_moving_forward = false;
	bool is_moving_backward = false;

	float nextRot = 0;
	//float finalRot = 0;
	float turn_angle = 0;
	float move_step = 0;
	float steps = 0;

    // Use this for initialization
    void Start () 
	{
		q = Quaternion.AngleAxis(rotation, Vector3.up);
		rayFront = new Ray (gameObject.transform.position, Vector3.forward);
		rayBack = new Ray (gameObject.transform.position, Vector3.back);
		//rayLeft = new Ray (gameObject.transform.position, Vector3.left);
		//rayRight = new Ray (gameObject.transform.position, Vector3.right);

		turn_angle = rotation / turn_speed;
		move_step = cell_size / move_speed;
		steps = cell_size;
	}

	void OnDrawGizmos() 
	{
		Gizmos.color = ray_col;
		Gizmos.DrawRay (rayFront);
		Gizmos.color = Color.blue;
		Gizmos.DrawRay (rayBack);
	}

	RaycastHit[] hittedObject;

	void CheckEnemyHit()
	{
		hittedObject = Physics.RaycastAll (rayFront, cell_size);
        
        Array.Sort(hittedObject, (x, y) => x.distance.CompareTo(y.distance));

		foreach(RaycastHit h in hittedObject)
		{
			if(h.collider != null)
			{
				if(h.collider.tag == "Wall")
				{
					//A wall is in front of you don't check over it
					break;
				}
				if(h.collider.tag == "Enemy")
				{
					Enemy collidedEnemy = h.collider.gameObject.GetComponent<Enemy> ();
					if(collidedEnemy != null)
					{
						GameController.instance.EnterCombat (collidedEnemy);
						break;
					}
				}
				else if(h.collider.tag == "Door")
				{
					if(GameController.instance.player.has_key == true)
					{
						//GameController.instance.ShowMessage ("Door opened", 2f);
						GameController.instance.Win ();
					}
					else
					{
						GameController.instance.ShowMessage ("The door is locked", 2f);
					}
				}
				else if(h.collider.tag == "Key")
				{
					if(GameController.instance.player.has_key == false)
					{
						GameController.instance.ShowMessage ("Key obtained", 1f);
						GameController.instance.player.has_key = true;
						h.collider.gameObject.SetActive (false);
					}
				}
			}
		}
	}

	// Update is called once per frame
	void Update () 
	{
		bool hitFront = Physics.Raycast (rayFront, cell_size);
		bool hitBack = Physics.Raycast (rayBack, cell_size);

		rayFront.origin = gameObject.transform.position;
		rayBack.origin = gameObject.transform.position;

		if(GameController.instance.state == GameController.GameStatus.GAME)
		{
			if(hitFront == true)
			{
				ray_col = Color.green;
			}
			else
			{
				ray_col = Color.red;
			}

			if(is_turning_right == true)
			{			
				if(nextRot <= 0)
				{
					is_turning_right = false;
					RotateCollisionRayRight ();
					CheckEnemyHit ();
				}
				else
				{
					gameObject.transform.Rotate (Vector3.up * turn_angle);
					nextRot -= 1;
					return;
				}
			}

			if(is_turning_left == true)
			{
				if(nextRot <= 0)
				{
					is_turning_left = false;
					RotateCollisionRayLeft ();
					CheckEnemyHit ();
				}
				else
				{
					gameObject.transform.Rotate (Vector3.down * turn_angle);
					nextRot -= 1;
					return;
				}
			}

			if(is_moving_forward == true)
			{
				if(steps <= 0)
				{
					is_moving_forward = false;
					CheckEnemyHit ();
					//gameObject.transform.position = Vector3.forward * cell_size;
				}
				else
				{
					transform.Translate (Vector3.forward * move_step * Time.deltaTime);
					steps -= move_step * Time.deltaTime;
					return;
				}
			}

			if(is_moving_backward == true)
			{
				if(steps <= 0)
				{
					is_moving_backward = false;
					//CheckEnemyHit ();
					//gameObject.transform.position = Vector3.forward * cell_size;
				}
				else
				{
					transform.Translate (Vector3.back * move_step * Time.deltaTime);
					steps -= move_step * Time.deltaTime;
					return;
				}
			}

			if(hitFront == false && Input.GetKey(KeyCode.W))
			{
				is_moving_forward = true;
				steps = cell_size;
			}
			else if(hitBack == false && Input.GetKey(KeyCode.S))
			{			
				//gameObject.transform.Translate (Vector3.back * cell_size);
				is_moving_backward = true;
				steps = cell_size;
			}
			else if(Input.GetKeyDown(KeyCode.A) ) //Left
			{
				is_turning_left = true;
				nextRot = turn_speed;
			}
			else if(Input.GetKeyDown(KeyCode.D) ) //Right
			{
				is_turning_right = true;
				nextRot = turn_speed;
			}
		}

		/*if(GameController.instance.state == GameController.GameStatus.COMBAT)
		{
			
		}*/

	}

	void EnterCombat()
	{
		//GameController.instance.EnterCombat ();
	}

	void RotateCollisionRayLeft()
	{
		direction = q* -direction;
		rayFront.direction = direction;
		rayBack.direction = -direction;
	}

	void RotateCollisionRayRight()
	{

		//Rotate the ray for collision
		direction = q * direction;
		rayFront.direction = direction;
		rayBack.direction = -direction;
	}

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.tag == "Wall")
    //    {
    //        //Debug.Log("wall collision on minimap");
    //        other.gameObject.layer = 8;
    //    }
    //}   
}
