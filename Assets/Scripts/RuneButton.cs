﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneButton : MonoBehaviour {

    public Button[] runes = new Button[5];
    private string magic_word = "";
    private string rune_sequence = "";
    private RuneSpell spell;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetRunes(string word, RuneSpell rune_spell)
    {
        spell = rune_spell;
        if (word.Length == runes.Length)
        {
            magic_word = word;

            string scrubled_word = ScrumbleString(word);
            Debug.Log("run scrumbled word: " + scrubled_word);
            Debug.Log("magic word: " + magic_word);

            for (int i = 0; i < runes.Length; i++)
            {
                Button btn = runes[i];
                Image button_image = btn.GetComponent<Image>();
                string rune = scrubled_word[i].ToString();
                
                button_image.type = Image.Type.Filled;
                button_image.fillMethod = Image.FillMethod.Horizontal;

                button_image.fillAmount = (float)0.01;

                btn.GetComponentInChildren<Text>().text = rune;

                btn.onClick.AddListener(delegate
                {
                    SelectRune(btn, rune);
                });
            }
        }
        else
        {
            Debug.LogError("Rune word longer than 5");
        }
    }

    string ScrumbleString(string s)
    {
        //string scramble = s;
        char[] start_string = s.ToCharArray();
        string res = "";
        string pos_filled = "";
        //char[] scramble = new char[s.Length];

        for (int i = 0; i < s.Length; i++)
        {
            int in_scramble = Random.Range(0, s.Length);
             
            while (pos_filled.Contains(in_scramble.ToString()) && pos_filled.Length < s.Length)
            {
                in_scramble = Random.Range(0, s.Length);
            }

            pos_filled += in_scramble.ToString();
            
            res += start_string[in_scramble];
        }
        return res;
    }

    //Set all the button to empty fill
    void ClearRunes()
    {
        rune_sequence = "";
        for (int i = 0; i < runes.Length; i++)
        {
            Button btn = runes[i];
            Image button_image = btn.GetComponent<Image>();
            
            button_image.fillAmount = (float)0.01;
        }
    }

    void SelectRune(Button btn, string rune)
    {
        rune_sequence += rune;

        Image button_image = btn.GetComponent<Image>();

        button_image.fillAmount = 1;

        if (rune_sequence == magic_word)
        {
            spell.Activate(Target.PLAYER);
            GameController.instance.ShowMessage("launched rune spell" + spell.a_name);
                        ClearRunes();
            SetRunes(magic_word, spell);
        }
        else if (magic_word.Substring(0, rune_sequence.Length) != rune_sequence)
        {
            ClearRunes();
        }
    }
}
