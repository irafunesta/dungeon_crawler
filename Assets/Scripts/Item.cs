﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum ItemType
{
	ARMOR,
	WEAPON,
	CONSUMABLE
}

/*
	 * 0 : Head
	 * 1 : Body
	 * 2 : Waist
	 * 3 : Arms
	 * 4 : Legs
	 * 5 : Feets
	 * 7 : Right Weapon
	 * 6 : Left Weapon
	*/
public enum EquipSlots
{
	NONE = -1,
	HEAD = 0,
	BODY = 1,
	WAIST = 2,
	ARMS = 3,
	LEGS = 4,
	FEETS = 5,
	RIGHT_WEAPON = 7,
	LEFT_WEAPON = 6
}

public class Item : MonoBehaviour{
	
	public ItemType Type;
	public EquipSlots equipSlot;
	public int id;
	public Sprite image;
	public string itemName;
	public string effect;

	//All item possible attributes and effects
	public int atkValue;
	public int defValue;
	public int spdValue;

	public int hpRestore;
}
