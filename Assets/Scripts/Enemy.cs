﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, ITarget {

	public int hp;
	public int maxHp;
	public int atk;
	public int def;
	public int spd = 2;

	public float cooldown = 1;

    public Action attack_action;

	// Use this for initialization
	void Start () {
		hp = maxHp;
        this.gameObject.tag = "Enemy";

        EventManager.GET_ENEMY_Listener += GetSelf;
    }

    Enemy GetSelf()
    {
        return this;
    }
    	
	// Update is called once per frame
	void Update () {
		
	}

	public void TargetOfSpell(Spell spell)
	{
		
	}

	public void Heal(int amount)
	{
		if(hp < maxHp)
		{
			hp = Mathf.Min(hp + amount, maxHp);
		}
	}

	public void Damage(int amount)
	{
		hp -= amount;
	}

    public int GetDamageBonus()
    {
        //TODO add other stats
        int bonus = 0;
        bonus = atk;
        return bonus;
    }

    public int GetDefenseBonus()
    {
        //TODO add other stats
        int bonus = 0;
        bonus = def;
        return bonus;
    }

    /*public void StartCombat() {
		if(cooldown <= 0)
		{
			
		}
	}*/
}
