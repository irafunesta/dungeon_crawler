﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToView : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//gameObject.transform.Rotate (Vector3.up);

		gameObject.transform.LookAt (Camera.main.gameObject.transform.position);
		gameObject.transform.Rotate (Vector3.up * 180);
	}
}
