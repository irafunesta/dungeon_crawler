﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour 
{
	public static GameController instance {private set; get;}

	public enum GameStatus {
		MENU,
		GAME,
		OPTION,
		COMBAT,
		GAME_OVER,
		WIN,
		INVENTORY
	}

	public GameStatus state;

	public GameObject menu;
	public GameObject HudScreen;
	public GameObject FightUi;
	public GameObject EnemyUi;
	public GameObject SlashEffect;
	public GameObject PlayerHit;
	public GameObject MessageContainer;

	public GameObject GameOverScreen;
	public GameObject WinScreen;
	public GameObject InventoryScreen;
	public GameObject PlayerDamageScreen;

	public Animator PortraitAnimator;
	public Animator CombatAnimator;

	public Text PlayerHp;
	public Text EnemyHp;
	public Image HealthBar;
	public Text MessageText;
	public Text DungeonTime;
	Text PlayerDamage;
    public Text[] CombatLog;

    [Header("InventoryButtons")]
    public GameObject ItemGrid;
    public GameObject ItemGridBack;

    public GameObject skillGrid;
    public GameObject skillGridBack;
    //public GameObject 

    [Header("Attack")]
	public Button[] CombatActions = new Button[6];
    public GameObject[] RuneActions = new GameObject[6];

    public Button base_attack;

    [Header("Portrait")]
    public Image PlayerATB;
	public Image EnemyATB;
	public Image PlayerPortrait;

    //Tooltip
    [Header("Tooltip")]
    public GameObject toolTip;
	public Text tpItemName;
	public Text tpItemType;
	public Text tpAtk;
	public Text tpDef;
	public Text tpSpd;
	public Text tpEffects;
	public float toolTipOffset = 30f;
    	
	bool enemy_can_attack = false;

	float game_start_time = 0;
	float time_passed = 0;

	public Player player;
	Enemy currentEnemy;

	float pl_turn_cooldown = 100;
	float en_turn_cooldown = 100;
	float total_pl_turn_cooldown = 100;
	float total_en_turn_cooldown = 100;

	public float turn_dec = 1;

	public string[] current_magic_seq = new string[6];

    public GameObject minimap;

	// Use this for initialization
	void Awake () 
	{
		if(instance != null && instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
 
        // Here we save our singleton instance
		instance = this;
        
		total_pl_turn_cooldown = CalculateATB(player.totalSpd);
		pl_turn_cooldown = total_pl_turn_cooldown;

		HealthBar.fillAmount = player.hp / player.maxHp;
		ColorHealthBar (player.hp / player.maxHp);

        //Initialize combat log
        CombatLog[0].text = "";
        CombatLog[1].text = "";
        
		//Add to inventory
        PlayerDamage = PlayerDamageScreen.GetComponentInChildren<Text> ();

		state = GameStatus.MENU;
		HudScreen.SetActive (false);
		menu.SetActive (true);
		FightUi.SetActive(true);
		EnemyUi.SetActive (false);
		PlayerHit.SetActive (false);
		GameOverScreen.SetActive (false);
		WinScreen.SetActive (false);
		InventoryScreen.SetActive (false);
		toolTip.SetActive (false);
		PlayerDamageScreen.SetActive (false);
        minimap.SetActive(false);

        skillGrid.SetActive(false);
        skillGridBack.SetActive(false);

        ItemGrid.SetActive(true);
        ItemGridBack.SetActive(true);

        EventManager.UiUpdateListener += UpdateCombatUi;
		EventManager.UI_SHOW_HEAL_NUMBER_Listener += ShowPlayerHeal;
        //EventManager.DAMAGE_ENEMY_Listener += DamageEnemy;
        //EventManager.SHOW_EFFECT_Listener += ShowEffect;
        EventManager.REMOVE_PLAYER_ACTION_Listener += RemoveAction;
        EventManager.DAMAGE_Listener += DamageEnemy;
        EventManager.DAMAGE_Listener += EndEnemyAttack;

        Debug.Log("target :" + Target.PLAYER.ToString());
    }

    //void ShowEffect(string effect)
    //{
    //    //SlashEffect.SetActive(true);
    //}

    public void ShowItemGrid()
    {
        skillGrid.SetActive(false);
        skillGridBack.SetActive(false);

        ItemGrid.SetActive(true);
        ItemGridBack.SetActive(true);
    }

    public void ShowSkillGrid()
    {
        skillGrid.SetActive(true);
        skillGridBack.SetActive(true);

        ItemGrid.SetActive(false);
        ItemGridBack.SetActive(false);
    }

    void RemoveAction(string name, int index)
    {
        Button btn = CombatActions[index];

        Image button_image = btn.GetComponent<Image>();
        btn.GetComponentInChildren<Text>().text = "";
        button_image.fillAmount = 1;

        btn.onClick.RemoveAllListeners();
        btn.gameObject.SetActive(false);

        //SetPlayerActions();
    }
    
	float CalculateATB(float spd)
	{
		return Mathf.Max(10, 100 - spd);
	}

    void ClearAllButtons()
    {
        for (int i = 0; i < CombatActions.Length; i++)
        {
            Button btn = CombatActions[i];
            btn.GetComponent<Image>().fillAmount = 0;
        }
    }

    void ClearAllSequence()
    {
        for (int i = 0; i < current_magic_seq.Length; i++)
        {
            current_magic_seq[i] = "";
        }
    }

    void CheckMagicInput()
	{
		//Catch keyboard input 
		string inputed = Input.inputString;
        List<Action> spell_to_launch = new List<Action>();
        
		if (inputed != "" && inputed != " " && Regex.IsMatch(inputed, "[a-z]"))
		{
			for(int i=0; i < player.actions.Length; i++)
			{
				Action act = player.actions [i];
				Button btn = CombatActions [i];
				if(act == null)
				{
					continue;
				}

				if (act.type == ActionType.SPELL)
				{
					Spell curretSpl = (Spell)act;
					current_magic_seq[i] += inputed.Trim();

					float amount = (float)(1f / curretSpl.spell_word.Length);
					btn.GetComponent<Image> ().fillAmount += amount;

					if (current_magic_seq[i] == curretSpl.spell_word)
					{
                        spell_to_launch.Add(act);	
					}
					else if (curretSpl.spell_word.Substring (0, current_magic_seq[i].Length) != current_magic_seq[i])
					{
						//Debug.Log ("error wrong letter");
						//					ShowMessage ("wrong", 0.1f);
						current_magic_seq[i] = "";  
                        //					ClearMagic ();
                        btn.GetComponent<Image> ().fillAmount = 0;
					}
				}
			}

            //Launch all the needed spells clear the rest
            foreach(Action act in spell_to_launch)
            {
                Spell s = (Spell)act;
                ShowMessage ("Use spell " + s.a_name, 1);

                //EventManager.EmitEvent (GameEvent.USE_SPELL, act);
                s.Activate(Target.PLAYER);
                EventManager.EmitEvent (GameEvent.UPDATE_UI, null);

                ClearAllSequence();
                ClearAllButtons();
            }
        }
    }

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(state == GameStatus.GAME)
			{
				//Open the menu for options and ecc.
				state = GameStatus.OPTION;
				menu.SetActive (true);
				HudScreen.SetActive (false);
                minimap.SetActive(false);

            }
			else if(state == GameStatus.OPTION)
			{
				state = GameStatus.GAME;
				menu.SetActive (false);
				HudScreen.SetActive (true);
                minimap.SetActive(true);
            }
		}

		if (state == GameStatus.COMBAT)
		{			
			if (en_turn_cooldown > 0)
			{
				en_turn_cooldown -= currentEnemy.cooldown * Time.deltaTime;
			}
			else
			{
				enemy_can_attack = true;
			}
//			
			//ATB Hud
			PlayerATB.fillAmount = pl_turn_cooldown / total_pl_turn_cooldown;
			EnemyATB.fillAmount = en_turn_cooldown / total_en_turn_cooldown;

			CheckMagicInput ();
		}

		if(state == GameStatus.COMBAT) //Enemy turn logic
		{
			if(currentEnemy.hp > 0)
			{
				if(enemy_can_attack == false)
				{
					return;
				}
				//Enemy attack

				PlayerHit.SetActive (true);
                currentEnemy.attack_action.Activate(Target.PLAYER);
				enemy_can_attack = false;
                en_turn_cooldown = total_en_turn_cooldown;
            }
			else
			{
				//Enemy defeated
				//Ruby.SetActive(false);
				PassTurn ();
				ExitCombat();
			}
		}

        ProcessMessageQueue();

        if ((state == GameStatus.GAME || state == GameStatus.COMBAT) && player.hp <= 0)
        {
            GameOver();
        }
    }

    void ProcessMessageQueue()
    {
        if (message_queue.Count > 0 && MessageContainer.activeSelf == false)
        {
            Debug.Log("Process message: " + message_queue[0]);
            MessageContainer.SetActive(true);
            MessageText.text = message_queue[0];
            CombatLog[0].text = message_queue[0];
            Invoke("HideMessage", 1);
            message_queue.RemoveAt(0);
        }
    }

	void RemoveEnemy()
	{
		currentEnemy.gameObject.SetActive (false);
	}

	public void Win()
	{
		state = GameStatus.WIN;
		time_passed = Time.time - game_start_time;
		DungeonTime.text = "Completed in " + time_passed.ToString ("##.000");
		WinScreen.SetActive (true);
	}

	public void GameOver ()
	{
		if((state == GameStatus.GAME || state == GameStatus.COMBAT))
		{
			state = GameStatus.GAME_OVER;
			GameOverScreen.SetActive (true);
            minimap.SetActive(false);
        }
	}

	public void Restart()
	{
		SceneManager.LoadScene (0);
	}

	void ColorHealthBar(float barPercent)
	{
		// red < 30%  yellow 31-60, green > 60
		Color col = Color.green;
		if(barPercent > 0.6f)
		{
			col = Color.green;
		}
		else if(barPercent < 0.6f && barPercent > 0.3f)
		{
			col = Color.yellow;
		}
		else if(barPercent < 0.3f)
		{
			col = Color.red;
			PlayerPortrait.color = col;
		}

		HealthBar.color = col;
		//PlayerPortrait.color = col;
	}

	void UpdateCombatUi()
	{
		//PlayerHp.text = player.hp.ToString () + "/" + player.maxHp.ToString ();
		//print ((player.hp / player.maxHp).ToString ());
		float barPercent = player.hp / player.maxHp;

		ColorHealthBar (barPercent);
		HealthBar.fillAmount = barPercent;

		if(currentEnemy != null)
		{
            if (currentEnemy.hp < 0)
            {
                EnemyHp.text = "DEAD";
            }
            else
            {
                EnemyHp.text = currentEnemy.hp.ToString() + "/" + currentEnemy.maxHp.ToString();
            }
        }		
	}

	public void EnterCombat (Enemy ene)
	{
		state = GameStatus.COMBAT;
//		turn = -1;
//		waiting_turn = true;

		total_pl_turn_cooldown = CalculateATB(player.totalSpd);
		pl_turn_cooldown = total_pl_turn_cooldown;

		currentEnemy = ene;

		total_en_turn_cooldown = CalculateATB (currentEnemy.spd) + 2;
		en_turn_cooldown = total_en_turn_cooldown;

		currentEnemy.gameObject.transform.Translate (Vector3.back * 1); //Make the enemy closer,The quad is rotate 180

		UpdateCombatUi ();
		//Show Combat ui
		EnemyUi.SetActive (true);
		//FightUi.SetActive(true);
//		FightButton.GetComponent<Image> ().fillAmount = 0.01f;
//		MagicButton.GetComponent<Image> ().fillAmount = 0.01f;

		//Ui animation
		PortraitAnimator.SetTrigger ("Enter");
		CombatAnimator.SetTrigger ("Enter");

//		current_magic_seq = "";
		for(int i = 0; i < current_magic_seq.Length; i++)
		{
			current_magic_seq[i] = "";
		}
		SetPlayerActions ();

        //Clear combat log
        CombatLog[0].text = "";
        CombatLog[1].text = "";
    }

	public void SetPlayerActions()
	{
		for(int i=0; i < player.actions.Length; i++)
		{
			Action act = player.actions [i];
			Button btn = CombatActions [i];
            GameObject btnRune = RuneActions[i];
			Image button_image = btn.GetComponent<Image> ();
			btn.GetComponentInChildren<Text> ().text = "";
			button_image.fillAmount = 1;

            btn.gameObject.SetActive(false);

            if (act != null)
			{
                btn.gameObject.SetActive(true);
                if (act.type == ActionType.SPELL)
				{
					button_image.type = Image.Type.Filled;
					button_image.fillMethod = Image.FillMethod.Horizontal;

					button_image.fillAmount = 0;

					btn.GetComponentInChildren<Text> ().text = act.a_name;
				}
				else if (act.type == ActionType.SKILL)
				{
					button_image.type = Image.Type.Filled;
					button_image.fillMethod = Image.FillMethod.Horizontal;

					button_image.fillAmount = 0;

					btn.onClick.AddListener (delegate
					{
						ChargeSkillAttack (btn, (Skill)act);
					});

					btn.GetComponentInChildren<Text> ().text = act.a_name;
				}
                else if (act.type == ActionType.RUNE_SPELL)
                {
                    //disable normal button
                    btn.gameObject.SetActive(false);
                    //enable rune button
                    btnRune.SetActive(true);

                    RuneSpell rune_spell = (RuneSpell)act;

                    RuneButton rune_button = btnRune.GetComponent<RuneButton>();
                    rune_button.SetRunes(rune_spell.runinc_spell_word, rune_spell);
                }
                else if (act.type == ActionType.ITEM)
                {
                    //				Image button_image = btn.GetComponent<Image> ();
                    button_image.type = Image.Type.Filled;
                    button_image.fillMethod = Image.FillMethod.Horizontal;

                    button_image.fillAmount = 0;
                    int button_index = i;

                    //TODO add the quantity from the inventory
                    int count = player.CountInventoryItem(act.a_name);

                    if(count > 0)
                    {
                        btn.onClick.AddListener(delegate
                        {
                            ChargeItemAttack(btn, (ItemScriptable)act, button_index);
                        });


                        btn.GetComponentInChildren<Text>().text = act.a_name + " x" + count.ToString();
                    }
                }
            }
		}

        //Base Attack
        //Image bbutton_image = base_attack.GetComponent<Image>();
        //bbutton_image.type = Image.Type.Filled;
        //bbutton_image.fillMethod = Image.FillMethod.Horizontal;

        //bbutton_image.fillAmount = 0;

        base_attack.onClick.AddListener(delegate
        {
            player.base_attack.Activate(Target.CURRENT_ENEMY);
            //ChargeSkillAttack(base_attack, (Skill)player.base_attack);
        });

    }

	public void ExitCombat()
	{
		state = GameStatus.GAME;
//		turn = -1;
		//Show Combat ui
		//FightUi.SetActive(false);
		EnemyUi.SetActive (false);

		PortraitAnimator.SetTrigger ("Exit");
		CombatAnimator.SetTrigger ("Exit");

		//TODO Win screen
	}

	public void ChargeSkillAttack(Button buttonIndex, Skill currentSkill)
	{
		Image button1_image = buttonIndex.GetComponent<Image> ();

		if(button1_image.fillAmount < 1)
		{
			//Increase the button fill by an amount ( 2click)
			button1_image.fillAmount +=  (1f / currentSkill.click_to_active);
		}
		else if(button1_image.fillAmount >= 1)
		{
            //ShowMessage("Use skill " + currentSkill.a_name, 1);
            currentSkill.Activate (Target.CURRENT_ENEMY);

			button1_image.fillAmount = 0.01f;
			UpdateCombatUi ();
		}
	}

    public void ChargeItemAttack(Button buttonIndex, ItemScriptable item, int index)
    {
        Image button1_image = buttonIndex.GetComponent<Image>();

        if (button1_image.fillAmount < 1)
        {
            //Increase the button fill by an amount ( 2click)
            button1_image.fillAmount += (1f / item.click_to_active);
        }
        else if (button1_image.fillAmount >= 1)
        {
            ShowMessage("Use skill " + item.a_name, 1);
            int count = player.CountInventoryItem(item.a_name);
            if (count > 0)
            {
                item.Activate(Target.PLAYER);

                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("item_name", item.a_name);
                data.Add("action_index", index.ToString());

                EventManager.EmitEvent(GameEvent.REMOVE_INVENTORY_ITEM, data);
                count = player.CountInventoryItem(item.a_name);
                if (count > 0)
                {
                    buttonIndex.GetComponentInChildren<Text>().text = item.a_name + " x" + count.ToString();
                }
                else
                {
                    EventManager.EmitEvent(GameEvent.REMOVE_PLAYER_ACTION, data);
                }
            }

            button1_image.fillAmount = 0.01f;
            UpdateCombatUi();
        }
    }

    public void DamageEnemy(int amount, string target)
    {
        if (target == Target.CURRENT_ENEMY.ToString())
        {
            //int damage = (player.totalAtk - currentEnemy.def) + Random.Range(1, 20);
            currentEnemy.hp -= amount;
            Debug.Log("player damaged for: " + amount);
            ShowMessage("Enemy take <color=red>" + amount.ToString() + "</color> damage.");
        }
    }

	public void EndAttackEffect()
	{
		//int damage = (player.totalAtk - currentEnemy.def) + Random.Range (1, 20);
  //      currentEnemy.hp -= damage;
  //      print("Damage:" + damage.ToString());
        //PassTurn ();

        SlashEffect.SetActive (false);
		PlayerHit.SetActive (false);
		//
		UpdateCombatUi ();
	}

    public void EndEnemyAttackEffect()
    {
        print("EndEnemyAttackEffect");
        SlashEffect.SetActive(false);
        PlayerHit.SetActive(false);
        //en_turn_cooldown = total_en_turn_cooldown;
    }

    public void EndEnemyAttack (int damage, string target)
	{
        print("end enemyAttack");
        if(target != Target.PLAYER.ToString())
        {
            return;
        }
		ShowPalyerDamage (damage);
//		PassTurn ();

		SlashEffect.SetActive (false);
		PlayerHit.SetActive (false);
		en_turn_cooldown = total_en_turn_cooldown;
		UpdateCombatUi ();
	}

	public void Item()
	{

	}

	public void PassTurn ()
	{
		if(currentEnemy.hp <= 0)
		{				
			RemoveEnemy ();
			ExitCombat ();
		}
        		
		SlashEffect.SetActive (false);
		PlayerHit.SetActive (false);
		//
		UpdateCombatUi ();
	}

	void ShowPalyerDamage(int damage)
	{
		if(PlayerDamageScreen.activeSelf == false)
		{
			PlayerDamageScreen.SetActive (true);
			PlayerDamage.color = Color.white;
			PlayerDamage.text = damage.ToString ();
			Invoke ("HidePlayerDamage", 1);
		}
	}

	void ShowPlayerHeal(int heal)
	{
		if(PlayerDamageScreen.activeSelf == false)
		{
			PlayerDamageScreen.SetActive (true);
			PlayerDamage.color = Color.green;
			PlayerDamage.text = heal.ToString ();
			Invoke ("HidePlayerDamage", 1);
		}
	}

	public void HidePlayerDamage()
	{
		PlayerDamageScreen.SetActive (false);
	}

	public void StartGame()
	{
		//print ("asdasd");
		state = GameStatus.GAME;

		menu.SetActive(false);
		HudScreen.SetActive (true);
		time_passed = 0;
		game_start_time = Time.time;
        minimap.SetActive(true);
    }
    
    //TODO move to top
    List<string> message_queue = new List<string>();

	public void ShowMessage (string message, float time = 1)
	{
        //message_queue.Add(message);
        //if(MessageContainer.activeSelf == false)
        //{
        //	MessageContainer.SetActive (true);
        //	MessageText.text = message;
        //	Invoke ("HideMessage", time);
        //}
        CombatLog[1].text = CombatLog[0].text;
        CombatLog[0].text = message;
    }

	public void ClearMagic()
	{
//		MaWgicButton.GetComponent<Image> ().fillAmount = 0;
	}

	public void HideMessage ()
	{
		MessageContainer.SetActive (false);
	}

	public void EndGame()
	{
		Application.Quit ();
	}

	public void OpenInventory()
	{
		if(state == GameStatus.COMBAT)
		{
			return;
		}

		if(InventoryScreen.activeSelf == true && state == GameStatus.INVENTORY)
		{
			InventoryScreen.SetActive (false);
			state = GameStatus.GAME;
            minimap.SetActive(true);
        }
		else
		{
			InventoryScreen.SetActive (true);
            minimap.SetActive(false);
            player.UpdateInvetory ();
			state = GameStatus.INVENTORY;
		}
	}

	public void DefaultItemAction(int itemSlotNum)
	{
		ItemScriptable selectedItem = player.inventory [itemSlotNum].item;

		if(selectedItem.Type == ItemType.ARMOR || selectedItem.Type == ItemType.WEAPON)
		{
			EquipItem (itemSlotNum);
		}
		else
		{
			//TODO Use item
			if(selectedItem.hpRestore > 0)
			{
				//if(player.hp < player.maxHp)
				//{
				//	player.hp = Mathf.Min(player.hp + selectedItem.hpRestore, player.maxHp);
				//	ShowMessage ("Using the item", 1);
				//	//Remove the item
				//	player.inventory [itemSlotNum] = null;
				//}

                selectedItem.Activate(Target.PLAYER);
                //reduce quantity
                player.inventory[itemSlotNum].quantity--;
			}
		}

		player.UpdateInvetory ();
        player.CalculateTotalPlayerStats();
    }

    public void MagicHeal()
	{
		if(player.hp < player.maxHp)
		{
			player.hp = Mathf.Min(player.hp + 20, player.maxHp);
			UpdateCombatUi ();
		}
	}

	void EquipItem(int itemSlotNum)
	{
		ItemScriptable selectedItem = player.inventory [itemSlotNum].item;
		if(selectedItem != null)
		{
			int itemEquipSlot = (int)selectedItem.equipSlot;
			//Goes to equip
			if(player.equipment [itemEquipSlot] == null)
			{
                //Can be equipped
                //Equip the item
                player.equipment[itemEquipSlot] = selectedItem;                
                player.inventory[itemSlotNum].item = null;
			}
			else
			{
				//Implement swap
				ItemScriptable swap = player.inventory [itemSlotNum].item;
				player.inventory [itemSlotNum].item = player.equipment [itemEquipSlot];
				player.equipment [itemEquipSlot] = swap;
			}
		}

		player.UpdateInvetory ();
		player.CalculateTotalPlayerStats ();
	}

	public void UnEquipItem(int equipSlotNum)
	{
		//TODO 
		//Find an empty slot in inventory
		int emptySlot = player.GetEmptyInventorySlot ();
		int eSlot = (int)equipSlotNum;

		if(emptySlot != -1)
		{
			//There is space
			//Remove the equip, add to inventory
			player.inventory [emptySlot].item = player.equipment [eSlot];
			player.equipment[eSlot] = null;
		}
		else
		{
			//Say message no more space
			GameController.instance.ShowMessage ("No space in inventory", 1);
		}

		player.UpdateInvetory ();
		player.CalculateTotalPlayerStats ();
	}

	public void ShowTooltip(int itemSlot, bool equip, Vector2 pos)
	{
		ItemScriptable selectedItem = null; 

		if(equip == true)
		{
			selectedItem = player.equipment [itemSlot];
		}
		else
		{
			selectedItem = player.inventory [itemSlot].item;
		}

		if(selectedItem != null)
		{
			toolTip.SetActive (true);

			tpItemName.text = selectedItem.itemName;

			if(selectedItem.equipSlot == EquipSlots.NONE)
			{
				tpItemType.text = selectedItem.Type.ToString ().ToLower();
			}
			else
			{
				tpItemType.text = selectedItem.equipSlot.ToString ().ToLower();
			}

			tpAtk.text = "ATK +" + selectedItem.atkValue;
			tpDef.text = "DEF +" + selectedItem.defValue;
			tpSpd.text = "SPD +" + selectedItem.spdValue;
			tpEffects.text = selectedItem.effect;

			/*RectTransform tooltipTransform = toolTip.GetComponent<RectTransform> ();
			if(tooltipTransform != null)
			{
				tooltipTransform.anchoredPosition = pos;
			}*/
		}
		else
		{
			Debug.Log("The selected slot is empty");
			toolTip.SetActive (false);
		}
	}

	public void HideTooTip()
	{
		toolTip.SetActive (false);
	}

	public void TestIncreaseScriptable(int slotNum)
	{
		ItemScriptable selectedItem = null; 


		selectedItem = player.inventory [slotNum].item;

		selectedItem.atkValue++;
	}
}
