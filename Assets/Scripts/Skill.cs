﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Skill", menuName = "Skill", order = 1)]
public class Skill : Action
{
	public int s_damage;
	public int s_heal;
	public int click_to_active;
	//public TargetType target;
	public DamageType s_damage_type;
    public EffectsName effect_name;
    public GameEvent[] event_trigger;
    public int dice_faces = 4;
    public int dice_num = 1;

    public override void Activate(Target currentTarget)
	{
        int total_damage = s_damage;
        //string target = EventManager.GetTargetString(currentTarget);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("heal_amount", s_heal.ToString());

        Player p = EventManager.GetPlayerData();
        Enemy en = EventManager.GetEnemyData();

        switch (currentTarget)
        {
            case Target.PLAYER: //Player as target player in def
                if (p != null && en != null)
                {
                    total_damage = CombatFormulas.CalculateDamage(dice_faces, dice_num, en.GetDamageBonus(), p.GetDamageBonus());
                }
                break;
            case Target.CURRENT_ENEMY: //Enemy as target enemy in def
                if (p != null && en != null)
                {
                    total_damage = CombatFormulas.CalculateDamage(dice_faces, dice_num, p.GetDamageBonus(), en.GetDamageBonus());
                }
                break;
        }
        data.Add("damage_amount", total_damage.ToString());
        data.Add("target", currentTarget.ToString());
        data.Add("effect_name", effect_name.ToString());
        
        foreach (GameEvent e in event_trigger)
        {
            EventManager.EmitEvent(e, data);
        }
    }
}

