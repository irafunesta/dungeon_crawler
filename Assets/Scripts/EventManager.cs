﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Target
{
    NONE,
    CURRENT_ENEMY,
    PLAYER
}

public enum GameEvent
{
	USE_SPELL,
	CURE_PLAYER,
	UPDATE_UI,
	UI_SHOW_HEAL_NUMBER,
    DAMAGE_ENEMY,
    DAMAGE,
    SHOW_EFFECT,
    REMOVE_INVENTORY_ITEM,
    REMOVE_PLAYER_ACTION,
    GET_PLAYER,
    GET_ENEMY
}

public class EventManager : MonoBehaviour
{
    public delegate void SpellLaunched(Spell sp);
    public static event SpellLaunched UseSpell;

    public delegate void CurePlayer(int amount);
    public static event CurePlayer CPlayer;

    public delegate void UiUpdate();
    public static event UiUpdate UiUpdateListener;

    public delegate void UI_SHOW_HEAL_NUMBER(int amount);
    public static event UI_SHOW_HEAL_NUMBER UI_SHOW_HEAL_NUMBER_Listener;

    public delegate void DAMAGE_ENEMY(int amount);
    public static event DAMAGE_ENEMY DAMAGE_ENEMY_Listener;

    public delegate void DAMAGE(int amount, string target);
    public static event DAMAGE DAMAGE_Listener;

    public delegate void SHOW_EFFECT(string name);
    public static event SHOW_EFFECT SHOW_EFFECT_Listener;

    public delegate void REMOVE_INVENTORY_ITEM(string name);
    public static event REMOVE_INVENTORY_ITEM REMOVE_INVENTORY_ITEM_Listener;

    public delegate void REMOVE_PLAYER_ACTION(string name, int index);
    public static event REMOVE_PLAYER_ACTION REMOVE_PLAYER_ACTION_Listener;

    public delegate Player GET_PLAYER();
    public static event GET_PLAYER GET_PLAYER_Listener;

    public delegate Enemy GET_ENEMY();
    public static event GET_ENEMY GET_ENEMY_Listener;

    //public static void EmitEvent(GameEvent e, Action act, int amount = 0)
    //{
    //    switch (e)
    //    {
    //        case GameEvent.USE_SPELL:
    //            if (UseSpell != null)
    //            {
    //                Spell s = (Spell)act;
    //                UseSpell(s);
    //            }
    //            break;
    //        case GameEvent.CURE_PLAYER:
    //            if (CPlayer != null)
    //                CPlayer(amount);
    //            break;
    //        case GameEvent.UPDATE_UI:
    //            if (UiUpdateListener != null)
    //                UiUpdateListener();
    //            break;
    //        case GameEvent.UI_SHOW_HEAL_NUMBER:
    //            if (UI_SHOW_HEAL_NUMBER_Listener != null)
    //                UI_SHOW_HEAL_NUMBER_Listener(amount);
    //            break;
    //        case GameEvent.DAMAGE_ENEMY:
    //            if (DAMAGE_ENEMY_Listener != null)
    //                DAMAGE_ENEMY_Listener(amount);
    //            break;
    //        case GameEvent.SHOW_EFFECT:
    //            if (SHOW_EFFECT_Listener != null)
    //                SHOW_EFFECT_Listener();
    //            break;
    //    }
    //}

    public static void EmitEvent(GameEvent e, Dictionary<string, string> data)
    {
        switch (e)
        {
            //case GameEvent.USE_SPELL:
            //    if (UseSpell != null)
            //    {
            //        Spell s = (Spell)act;
            //        UseSpell(s);
            //    }
            //    break;
            case GameEvent.CURE_PLAYER:
                if (CPlayer != null)
                {
                    string amount;
                    data.TryGetValue("heal_amount", out amount);
                    CPlayer(int.Parse(amount));
                }
                break;
            case GameEvent.UPDATE_UI:
                if (UiUpdateListener != null)
                    UiUpdateListener();
                break;
            case GameEvent.UI_SHOW_HEAL_NUMBER:
                if (UI_SHOW_HEAL_NUMBER_Listener != null)
                {
                    string amount;
                    data.TryGetValue("heal_amount", out amount);
                    UI_SHOW_HEAL_NUMBER_Listener(int.Parse(amount));
                }
                break;
            case GameEvent.DAMAGE_ENEMY:
                if (DAMAGE_ENEMY_Listener != null)
                {
                    string amount;
                    data.TryGetValue("damage_amount", out amount);
                    DAMAGE_ENEMY_Listener(int.Parse(amount));
                }
                break;
            case GameEvent.DAMAGE:
                if (DAMAGE_Listener != null)
                {
                    string amount;
                    string target;
                    data.TryGetValue("damage_amount", out amount);
                    data.TryGetValue("target", out target);
                    DAMAGE_Listener(int.Parse(amount), target);
                }
                break;
            case GameEvent.SHOW_EFFECT:
                if (SHOW_EFFECT_Listener != null)
                {
                    string effect_name;
                    data.TryGetValue("effect_name", out effect_name);
                    if (effect_name != null)
                    {
                        SHOW_EFFECT_Listener(effect_name);
                    }
                }
                break;
            case GameEvent.REMOVE_INVENTORY_ITEM:
                if (REMOVE_INVENTORY_ITEM_Listener != null)
                {
                    string item_name;
                    data.TryGetValue("item_name", out item_name);
                    REMOVE_INVENTORY_ITEM_Listener(item_name);
                }
                break;
            case GameEvent.REMOVE_PLAYER_ACTION:
                if (REMOVE_PLAYER_ACTION_Listener != null)
                {
                    string item_name;
                    string action_index;
                    data.TryGetValue("item_name", out item_name);
                    data.TryGetValue("action_index", out action_index);
                    REMOVE_PLAYER_ACTION_Listener(item_name, int.Parse(action_index));
                }
                break;
            case GameEvent.GET_PLAYER:
                if (GET_PLAYER_Listener != null)
                {
                    GET_PLAYER_Listener();
                }
                break;
        }
    }

    public static Player GetPlayerData()
    {
        Player p = null;
        if (GET_PLAYER_Listener != null)
        {
            p = GET_PLAYER_Listener();
        }
        return p;
    }

    public static Enemy GetEnemyData()
    {
        Enemy p = null;
        if (GET_ENEMY_Listener != null)
        {
            p = GET_ENEMY_Listener();
        }
        return p;
    }

    public static string GetTargetString(Target t)
    {
        string s = "";
        switch (t)
        {
            case Target.PLAYER:
                s = "Player";
                break;
            case Target.CURRENT_ENEMY:
                s = "Enemy";
                break;
        }
        return s;
    }
}
