﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Data", menuName = "Inventory/Item", order = 1)]
[System.Serializable]
public class ItemScriptable : Action 
{
    [Header("Item")]
    public ItemType Type;
	public EquipSlots equipSlot;
	public int id;
	public Sprite image;
	public string itemName;
	public string effect;
    public int click_to_active;
    public bool stackable = true;

    //All item possible attributes and effects
    public int atkValue;
	public int defValue;
	public int spdValue;
	public int hpRestore;

    public GameEvent[] event_trigger;

    public override void Activate(Target t)
    {
        //TODO do stuff
        if(Type == ItemType.CONSUMABLE)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("heal_amount", hpRestore.ToString());

            foreach (GameEvent e in event_trigger)
            {
                EventManager.EmitEvent(e, data);
            }
        }
    }
}

