﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


[Serializable]
public struct ItemSlot
{
    public ItemScriptable item;
    public int quantity;
}

[Serializable]
public struct EquipmentInfo
{
    public string requiredCategory;
    public string location;
    public string defaultItem;
}

public class Player : MonoBehaviour
{

	public float hp;
	public float maxHp;
	public int atk;
	public int def;
	public int spd;
	public bool has_key;

	public int totalAtk;
	public int totalDef;
	public int totalSpd;
	public float totalHp;

	//Equipemt slot =
	/*
	 * 0 : Head
	 * 1 : Body
	 * 2 : Waist
	 * 3 : Arms
	 * 4 : Legs
	 * 5 : Feets
	 * 7 : Right Weapon
	 * 6 : Left Weapon
	*/

	public ItemScriptable[] equipment = new ItemScriptable[8];
	//public ItemScriptable[] inventory = new ItemScriptable[24];
    //public InventorySlot[] inventory = new InventorySlot[24];

    //[Header("Equipment")]
    //public EquipmentInfo[] equipmentInfo = new EquipmentInfo[]
    //{
    //    new EquipmentInfo{requiredCategory="Weapon", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Head", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Chest", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Legs", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Shield", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Shoulders", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Hands", location=null, defaultItem=null},
    //    new EquipmentInfo{requiredCategory="Feet", location=null, defaultItem=null}
    //};

    public ItemSlot[] inventory = new ItemSlot[]
    {
        new ItemSlot{item=null, quantity = 0}
    };

    public GameObject inventoryGrid;
	public GameObject equipmentGrid;

    public Text txtTotalAtk;
	public Text txtTotalDef;
	public Text txtTotalSpd;
	public Text txtTotalHp;

	public bool invincible;

	public Action[] actions = new Action[6];
    public Action base_attack;

	// Use this for initialization
	void Start () {
		has_key = false;
        hp = maxHp;

        totalHp = hp;
		totalAtk = atk;
		totalDef = def;
		totalSpd = spd;

		CalculateTotalPlayerStats ();

		EventManager.CPlayer += Heal;
        EventManager.REMOVE_INVENTORY_ITEM_Listener += RemoveOneItem;
        EventManager.REMOVE_PLAYER_ACTION_Listener += RemoveAction;
        EventManager.GET_PLAYER_Listener += GetSelf;
        EventManager.DAMAGE_Listener += Damage;
    }

    void Damage(int amount, string target)
    {
        print("P hitted you for:" + amount.ToString());
        if (target != Target.PLAYER.ToString())
        {
            return;
        }
        if (invincible == false && Application.isEditor == true)
        {
            hp -= amount;
        }
        print("P hitted you for:" + amount.ToString());
        //GameController.instance.ShowMessage("P hitted for:" + amount.ToString());
        GameController.instance.ShowMessage("Player take <color=red>" + amount.ToString() + "</color> damage.");

        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("effect_name", EffectsName.PLAYER_DAMAGE.ToString());

        EventManager.EmitEvent(GameEvent.UPDATE_UI, null);
        EventManager.EmitEvent(GameEvent.SHOW_EFFECT, data);

        if (hp < 0)
        {
            GameController.instance.GameOver();
        }
    }

    Player GetSelf()
    {
        return this; 
    }
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		txtTotalHp.text = hp.ToString();
	}

	public void CalculateTotalPlayerStats()
	{
		totalAtk = atk;
		totalDef = def;
		totalSpd = spd;
		totalHp = hp;

		for (int i = 0; i < equipment.Length; i++)
		{
			ItemScriptable currentItem = equipment [i];
			if (currentItem != null)
			{
				if (currentItem.Type == ItemType.ARMOR || currentItem.Type == ItemType.WEAPON)
				{
					totalAtk += currentItem.atkValue;
					totalDef += currentItem.defValue;
					totalSpd += currentItem.spdValue;
					//totalHp += currentItem.
				}
			}
		}

		txtTotalHp.text = totalHp.ToString();
		txtTotalAtk.text = totalAtk.ToString ();
		txtTotalDef.text = totalDef.ToString ();
		txtTotalSpd.text = totalSpd.ToString ();

        EventManager.EmitEvent(GameEvent.UPDATE_UI, null);
    }

	public void UpdateInvetory()
	{
		//Change the inventoryGrid based on the inventory
		for(int i = 0; i < inventory.Length; i++)
		{
			Transform invSlot = inventoryGrid.transform.GetChild (i);
			Image invSlotImg = null;
			InventorySlot invSlotScript = null;

			if(invSlot != null)
			{
				invSlotImg = invSlot.gameObject.GetComponent<Image> ();
				invSlotScript = invSlot.gameObject.GetComponent<InventorySlot> ();
				invSlotScript.slotNum = i;
                invSlotScript.quantity.text = ""; 
            }

			ItemScriptable currentItem = inventory [i].item;

			if(invSlotImg != null)
			{
				if (currentItem != null)
				{
                    invSlotImg.enabled = true;
                    invSlotImg.sprite = currentItem.image;

                    if (currentItem.stackable == true)
                    {
                        if(inventory[i].quantity > 0)
                        {                            
                            invSlotScript.quantity.text = inventory[i].quantity.ToString();
                        }
                        else
                        {
                            //Ui slot
                            invSlotImg.enabled = false;
                            invSlotScript.quantity.text = "";
                            //Remove item from inventory
                            inventory[i].item = null;
                        }
                    }
                }
				else
				{
					invSlotImg.enabled = false;
					invSlotImg.sprite = null;
				}
			}
		}

		UpdateEquipment ();
	}

	void UpdateEquipment()
	{
		for(int i = 0; i < equipment.Length; i++)
		{
			Transform equipSlot = equipmentGrid.transform.GetChild (i);
			Image equipSlotImg = null;
			EquipSlot equipSlotScript = null;

			if(equipSlot != null)
			{
				equipSlotImg = equipSlot.gameObject.GetComponent<Image> ();
				equipSlotScript = equipSlot.gameObject.GetComponent<EquipSlot> ();
				equipSlotScript.slotNum = i;
			}

			ItemScriptable currentItem = equipment [i];

			if(equipSlotImg != null)
			{
				if (currentItem != null)
				{
					equipSlotImg.enabled = true;
					equipSlotImg.sprite = currentItem.image;
				}
				else
				{
					equipSlotImg.enabled = false;
					equipSlotImg.sprite = null;
				}
			}
		}
	}

	public int GetEmptyInventorySlot()
	{
		for (int i = 0; i < inventory.Length; i++)
		{
			if (inventory [i].item == null)
			{
				return i;
			}
		}

		return -1;
	}

    public int CountInventoryItem(string name)
    {
        int count = 0;
        for (int i = 0; i < inventory.Length; i++)
        {
            ItemScriptable item = inventory[i].item;
            if (item  != null && item.a_name == name)
            {
                if (item.stackable == true)
                {
                    return inventory[i].quantity;
                }
                else
                {
                    count++;
                }
            }
        }
        return count;
    }

    public void RemoveOneItem(string name)
    {
        if (name != "" && name != null)
        {
            for (int i = 0; i < inventory.Length; i++)
            {
                ItemScriptable item = inventory[i].item;
                if (item != null && item.a_name == name)
                {
                    if (item.stackable == true && inventory[i].quantity > 0)
                    {
                        inventory[i].quantity--;
                    }
                    else
                    {
                        inventory[i].item = null;
                    }
                    break;
                }
            }
        }
    }

    public void RemoveAction(string name, int index)
    {
        if (name != "" && name != null)
        {
            for (int i = 0; i < actions.Length; i++)
            {
                Action act = actions[i];
                if (act != null && act.a_name == name)
                {
                    actions[i] = null;
                    break;
                }
            }
        }
    }

    public void TargetOfSpell(Spell spell)
	{
		
	}

	public void Heal(int amount)
	{
        print("Healed player by:" + amount.ToString());
		if(hp < maxHp)
		{
			hp = Mathf.Min(hp + amount, maxHp);
		}
	}

	public int GetDamageBonus()
	{
        //TODO add Skill level bonus
        int bonus = 0;
        bonus = totalAtk;
        return bonus;
	}

    public int GetDefenseBonus()
    {
        //TODO add other stats
        int bonus = 0;
        bonus = totalDef;
        return bonus;
    }
}
