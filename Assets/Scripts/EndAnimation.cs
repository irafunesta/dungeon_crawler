﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAnimation : MonoBehaviour {

	public void EndPlayerAttack()
	{
		GameController.instance.EndAttackEffect ();
	}

	public void EndPlayerDamageAnim()
    {
        GameController.instance.EndEnemyAttackEffect();
    }
}
