﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatFormulas : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static int CalculateDamage(int dice_faces, int dice_num, int attacker_bonus, int defender_bonus)
    {
        int dam = 0;

        dam = Random.Range(1 * dice_num, dice_faces * dice_num + 1) + attacker_bonus - defender_bonus;

        return dam;
    }
}
