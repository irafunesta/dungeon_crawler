﻿using UnityEngine;
using System.Collections;

public interface ITarget 
{
	void TargetOfSpell (Spell spell);
//	void TargetOfItem (ItemScriptable item);
	void Heal(int amount);
	void Damage (int amount);
}

