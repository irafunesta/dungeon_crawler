﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WallTile : MonoBehaviour {

    public Material wall_material;
    public GameObject[] walls = new GameObject[4];
    public bool[] directions = new bool[4];

    const int north_wall = 0;
    const int est_wall = 1;
    const int south_wall = 2;
    const int west_wall = 3;

    // Use this for initialization
    void Start () {
        this.gameObject.tag = "Tile";
        this.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");

		for(int i =0; i< walls.Length; i++)
        {
            walls[i].GetComponent<QuadWall>().wall_material = wall_material;
            walls[i].SetActive(directions[i]);
        }

        //Adjust walls
        if(walls[north_wall].activeSelf == false)
        {
            //Move north the w and e walls
            //walls[est_wall].transform.tr
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Application.isEditor == true) //Used for show the tile on editor
        {
            for (int i = 0; i < walls.Length; i++)
            {
                //walls[i].GetComponent<MeshRenderer>().material = wall_material;
                if (walls[i] != null)
                {
                    walls[i].SetActive(directions[i]);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera") //Player
        {
            //Debug.Log("wall collision on minimap");
            for (int i = 0; i < walls.Length; i++)
            {
                //walls[i].GetComponent<MeshRenderer>().material = wall_material;
                walls[i].GetComponent<QuadWall>().minimap_sprite.layer = 8;
            }
        }
    }
}
