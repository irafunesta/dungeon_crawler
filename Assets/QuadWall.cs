﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadWall : MonoBehaviour {

    public Material wall_material;
    public GameObject minimap_sprite;

    // Use this for initialization
    void Start () {
        MeshRenderer[] faces = gameObject.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < faces.Length; i++)
        {
            faces[i].material = wall_material;
            //faces[i].gameObject.SetActive(directions[i]);
        }

        if (minimap_sprite != null)
        {
            minimap_sprite.layer = 9;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
