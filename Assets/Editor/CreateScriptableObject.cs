﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeScriptableObject {
	[MenuItem("Assets/Create/ItemScriptable")]
	public static void CreateMyAsset()
	{
		ItemScriptable asset = ScriptableObject.CreateInstance<ItemScriptable>();

		AssetDatabase.CreateAsset(asset, "Assets/Items/NewItem.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}